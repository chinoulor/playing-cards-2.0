#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum  Rank
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace,

};

enum Suit
{
	Hearts,
	Diamonds,
	Spades,
	Clovers
};


struct Card
{
	Suit Suit;
	Rank Rank;

};

void PrintCard(Card card);
Card HighCard(Card card1, Card card2);

int main()
{
	Card card1;
	card1.Rank = Jack;
	card1.Suit = Clovers;
	Card card2;
	card2.Rank = Ace;
	card2.Suit = Spades;

	PrintCard(card1);

	cout << "\nThe High Card is ";
	PrintCard(HighCard(card1, card2));
	
	_getch();
	return 0;
}

void PrintCard(Card card) {
	switch (card.Rank) {
	case Two: cout << "The Two of "; break;
	case Three: cout << "The Three of "; break;
	case Four: cout << "The Four of "; break;
	case Five: cout << "The Five of "; break;
	case Six: cout << "The Siz of "; break;
	case Seven: cout << "The Seven of "; break;
	case Eight: cout << "The Eight of "; break;
	case Nine: cout << "The Nine of "; break;
	case Jack: cout << "The Jack of "; break;
	case Queen: cout << "The Queen of "; break;
	case King: cout << "The King of "; break;
	case Ace: cout << "The Ace of "; break;
	}

	switch (card.Suit) {
	case Hearts: cout << "Hearts\n"; break;
	case Diamonds: cout << "Diamonds\n"; break;
	case Spades: cout << "Spades\n"; break;
	case Clovers: cout << "Clubs\n"; break;
	}
}

Card HighCard(Card card1, Card card2) {
	if (card1.Rank > card2.Rank) return card1;
	
	return card2;
}